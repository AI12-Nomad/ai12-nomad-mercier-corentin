import typing

from client.ihm.main.views.connection_view import ConnectionView
from common.interfaces.i_ihm_game_calls_ihm_main import I_IHMGameCallsIHMMain
from common.interfaces.i_comm_calls_ihm_main import I_CommCallsIHMMain
from common.interfaces.i_ihm_main_calls_comm import I_IHMMainCallsComm
from common.interfaces.i_ihm_main_calls_ihm_game import I_IHMMainCallsIHMGame

# from common.interfaces.i_ihm_main_calls_data import I_IHMMainCallsData


class IHMMainController:
    def __init__(
        self,
        pygame_controller: typing.Any,  # no type because of circular import
        my_interface_from_ihm_game: I_IHMGameCallsIHMMain = None,
        my_interface_from_comm: I_CommCallsIHMMain = None,
        my_interface_to_comm: I_IHMMainCallsComm = None,
        my_interface_to_ihm_game: I_IHMMainCallsIHMGame = None,
        # my_interface_to_data: I_IHMMainCallsData = None,
    ):
        self.pygame_controller = pygame_controller
        self.my_interface_from_ihm_game = my_interface_from_ihm_game
        self.my_interface_from_comm = my_interface_from_comm
        self.my_interface_to_comm = my_interface_to_comm
        self.my_interface_to_ihm_game = my_interface_to_ihm_game
        # self.my_interface_to_data = my_interface_to_data

        # initialize the first view
        self.connection_view = ConnectionView(
            pygame_controller.get_ui_manager(),
            pygame_controller.get_ui_renderer(),
            self,
        )
        pygame_controller.show_view(self.connection_view)

    def set_my_interface_to_comm(self, i: I_IHMMainCallsComm) -> None:
        self.my_interface_to_comm = i

    def set_my_interface_to_ihm_game(self, i: I_IHMMainCallsIHMGame) -> None:
        self.my_interface_to_ihm_game = i

    #    def set_my_interface_to_data(self, i: I_IHMMainCallsData) -> None:
    #        self.my_interface_to_data = i

    def get_my_interface_from_ihm_game(self) -> I_IHMGameCallsIHMMain:
        if self.my_interface_from_ihm_game is not None:
            return self.my_interface_from_ihm_game
        else:
            raise Exception(
                "get_my_interface_from_ihm_game was called but my_interface_from_ihm_game is None"
            )

    def get_my_interface_from_comm(self) -> I_CommCallsIHMMain:
        if self.my_interface_from_comm is not None:
            return self.my_interface_from_comm
        else:
            raise Exception(
                "get_my_interface_from_comm was called but my_interface_from_ihm_game is None"
            )
